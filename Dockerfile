FROM openjdk:8-jdk-alpine
ARG JAR_FILE=./target/*.jar
COPY ${JAR_FILE} app.jar
CMD useradd -r -u 999 appuser
USER appuser
ENTRYPOINT [“java”,”-jar”,”/app.jar”]